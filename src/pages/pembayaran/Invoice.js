import React, { Fragment, useContext, useEffect, useRef, useState } from "react";
//style
import { makeStyles, withStyles } from "@material-ui/core/styles";
//component
import AddBoxIcon from "@material-ui/icons/AddBox";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableFooter from "@material-ui/core/TableFooter";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Dialog from "../../components/Dialog/Dialog"
import GetAppIcon from "@material-ui/icons/GetApp";
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import { Link } from "react-router-dom";
import { EventContext } from "../../context/eventcontext";
import { ContingentContext } from "../../context/contingentcontext";
import { InvoiceContext } from "../../context/invoicecontext";
import Alert from '@material-ui/lab/Alert';
import ReactWaterMark from 'react-watermark-component';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import moment from 'moment'
const useStyles = makeStyles(() => ({
  title: {
    fontSize: "36px",
    fontWeight: "600",
  },
  boxTanggal: {
    marginTop: 30,
    marginRight: 30,
  },
  divInvoice: {
    marginTop: 30,
  },
  paper: {
    border: "1px solid #000",
    padding: 16,
  },
  boxNoFaktur: {
    marginTop: 30,
    textAlign: "center",
    fontSize: 24,
  },
  table: {
    minWidth: 650,
    "& th": {
      fontWeight: "600",
    },
  },
  boxLunas: {
    marginTop: 30,
    marginRight: 60,
    marginBottom: 20,
    color: "#3BB957",
  },
  boxKeterangan: {
    marginTop: 20,
    marginBottom: 20,
  },
}));

const StyledTableCell = withStyles((theme) => ({
  head: {
    color: theme.palette.common.black,
    fontSize: 16,
  },
  body: {
    fontSize: 14,
  },
  footer: {
    color: theme.palette.common.black,
    fontSize: 16,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

function createData(nomor, rincianBayar, kuantitas, nominal, subTotal) {
  return { nomor, rincianBayar, kuantitas, nominal, subTotal };
}
function createData2(nomor, kategori, perorangan, beregu) {
  return { nomor, kategori, perorangan, beregu };
}

const rows = [createData(1, "Peserta Open Perorangan", 175000, 2, 350000), createData(2, "Peserta Open Beregu", 300000, 1, 300000), createData(3, "Peserta Festival", 195000, 1, 195000)];

const rows2 = [createData2(1, "Open Turnament", 2, 1), createData2(2, "Festival", 1, 0)];




const Invoice = (props) => {
  const classes = useStyles();
  // var { getDataEvent, eventdata } = useContext(EventContext);
  var { getDataInvoiceDetail, confirm_invoice, putDataReceivePayment, putDataResetPayment, invoice_detail, entrie_detail } = useContext(InvoiceContext);
  // var { getDataContingent, contingent } = useContext(ContingentContext);
  const invoice_number =  props.match.params.invoice_number
  useEffect(() => {
    // getDataEvent()
    // getDataContingent();
    getDataInvoiceDetail(invoice_number);
  }, [])

  function currencyFormat(num) {
    return 'Rp. ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  }

  const [openModal, setopenModal] = useState(false)
  const [openModalReceivePayment, setopenModalReceivePayment] = useState(false)  
  const [documentFile, setdocumentFile] = useState()
  const docRef = useRef(null);
  const handleClose = () =>{
    setopenModal(false)
    setopenModalReceivePayment(false)
    setopenModalReset(false)
  }

  function handleInputFileDoc() {
    docRef.current.click();    
  }

  async function handleDocument(e) {
    try {
    console.log('data log', e.target.files[0])
    let reader = new FileReader();
    let file = e.target.files[0];
    setdocumentFile(file)
      reader.onloadend = () => {
        //setImg(compressedFile);
      };
      reader.readAsDataURL(file);

      const formData = new FormData();
      formData.append("document",file);

      console.log('form data : ',formData);
    } 
    catch (error) {
      console.log(error);
    }
  }

  async function uploadDokumen(){

    var data = {
      invoice_status : "Konfirmasi",
      payment_confirm : {
          bank_account : account_number,
          account_holder : account_name,
          transfer_date : transfer_date,
          //payment_file : String
      },
    }

    confirm_invoice(invoice_detail._id, data).then(data=>{
      props.history.goBack()
    })
    // var formData = new FormData();
    //   formData.append('document', documentFile)
      // var datapost = await postDataSK100(eventid, formData)
      // if(datapost){
      //   setopenModal(false)
      // }

    
  }

  const [account_number, setaccount_number] = useState("")
  const [account_name, setaccount_name] = useState("")
  const [transfer_date, settransfer_date] = useState("")
  const [payment_file, setpayment_file] = useState("")

  const [openModalReset, setopenModalReset] = useState(false)

  const options = {
    chunkWidth: 200,
    chunkHeight: 60,
    textAlign: 'left',
    textBaseline: 'bottom',
    globalAlpha: 0.17,
    font: '12px Microsoft Yahei',
    rotateAngle: -0.26,
    fillStyle: '#666'
  }

  const receivepayment = () =>{
    putDataReceivePayment(invoice_number).then(data=>{
      console.log('data', data)
      handleClose()
    })
  }

  const resetPayment = () =>{
    putDataResetPayment(invoice_number).then(data=>{
      console.log('data', data)
      handleClose()
    })
  }

  return (
    <Fragment>
      <Dialog
          open={openModal}
          close={handleClose}
          title={<Typography style={{color:'#bf272b'}}>Konfirmasi Pembayaran</Typography>}
          content={
            <>
              <Grid container spacing={1}>
                <Grid item md={12} xs={12}>
                  <Typography>Nomor Rekening</Typography>
                  <TextField value={account_number} onChange={(event)=>setaccount_number(event.target.value)} variant="outlined" fullWidth required />
                </Grid>
                <Grid item md={12} xs={12}>
                  <Typography>Nama Pengirim</Typography>
                  <TextField value={account_name} onChange={(event)=>setaccount_name(event.target.value)} variant="outlined" fullWidth required />
                </Grid>
                <Grid item md={12} xs={12}>
                  <Typography>Tanggal Pengiriman</Typography>
                  <TextField
                      id="tanggalLahir"
                      type="date"
                      value={transfer_date}
                      onChange={(event)=> {
                        settransfer_date(event.target.value);
                      }}
                      defaultValue={new Date()}
                      InputLabelProps={{
                        shrink: true,
                      }}
                      variant="outlined"
                      fullWidth
                      required
                    />
                </Grid>
                <Grid item md={12} xs={12}>
                  <Typography>Bukti Pembayaran</Typography>
                  <div ref={docRef} onClick={handleInputFileDoc} style={{width:'100%', backgroundColor:'#EFEFEF', borderWidth:1, borderColor:'#000', justifyContent:'center', alignContent:'center', alignItems:'center', alignSelf:'center'}}>
                    <center style={{padding:60}}>
                        <CloudUploadIcon style={{fontSize:38}}/>
                        <Typography></Typography>  
                    </center>
                  </div>
                </Grid>
              </Grid>
              <input type="file" ref={docRef} accept="application/pdf" style={{display: 'none'}} onChange={e=>handleDocument(e)}/>
            </>
          }
          cancel={handleClose}
          confirm={documentFile? uploadDokumen : null}
          valueConfirm={"Kirim Bukti Pembayaran"}
          valueCancel={"Batalkan"}
          colorButtonConfirm={"#bf272b"}
      />

      <Dialog
          open={openModalReceivePayment}
          close={handleClose}
          title={<Typography style={{color:'#bf272b'}}>Terima Pembayaran</Typography>}
          content={
            <>
              <Typography>Apakah Anda telah menerima pembayaran ini?</Typography>
            </>
          }
          cancel={handleClose}
          confirm={ receivepayment}
          valueConfirm={"Ya, Terima Pembayaran"}
          valueCancel={"Batalkan"}
          colorButtonConfirm={"#bf272b"}
      />

      <Dialog
          open={openModalReset}
          close={handleClose}
          title={<Typography style={{color:'#bf272b'}}>Reset Pembayaran</Typography>}
          content={
            <>
              <Typography>Apakah Anda yakin mereset pembayaran ini?</Typography>
            </>
          }
          cancel={handleClose}
          confirm={ resetPayment }
          valueConfirm={"Ya, Reset Pembayaran"}
          valueCancel={"Batalkan"}
          colorButtonConfirm={"#bf272b"}
      />
      <div className={classes.title}>Invoice</div>
      <div className={classes.divInvoice}>
        <Paper elevation={0} className={classes.paper}>

          {/* <ReactWaterMark
              waterMarkText={"Brilyan Sport Teknologi"}
              openSecurityDefense
              options={options}
            > */}
                <Box justifyContent="center" className={classes.boxNoFaktur}>
                  <Typography variant="h6" >
                    Invoice #{invoice_detail.invoice_number}
                  </Typography>
                  
                  <Chip
                      label={"status "+ invoice_detail.invoice_status == "" ? "Belum Bayar" : invoice_detail.invoice_status}
                      color="primary"
                    />
                </Box>
                <Alert severity="warning">Peserta yang telah didaftarkan paling lambat 2 hari saat tgl pendaftaran diharuskan untuk menyelesaikan pembayaran. Apabila peserta tidak membayar sampai batas waktu tersebut, sistem akan melakukan penghapus data secara otomatis.</Alert>
                <hr />

                
                <Box>
                  <Grid container spacing={2}>
                    <Grid item xs={12} xs={6}>
                      <p style={{ textAlign: "left", fontSize: 16 }}>
                        Pembayaran dari: <br />
                        Nama Kontingen : {invoice_detail.contingent_name} <br />
                        Nama Manajer : {invoice_detail.invoice_from? invoice_detail.invoice_from.name:"-"} <br />
                        Telp : {invoice_detail.invoice_from? invoice_detail.invoice_from.telp:"-"} <br />
                      </p>
                    </Grid>
                    <Grid item xs={12} xs={6}>
                      <p style={{ textAlign: "right", fontSize: 16 }}>
                        Pembayaran untuk: <br />
                        {invoice_detail.event_name} <br />
                        Rekening : {invoice_detail.invoice_to ? invoice_detail.invoice_to.bank_name + " - "+ invoice_detail.invoice_to.bank_account: "-"} <br />
                        a/n : {invoice_detail.invoice_to ? invoice_detail.invoice_to.name + " - "+ invoice_detail.invoice_to.telp : "-"}
                      </p>
                    </Grid>
                  </Grid>
                </Box>
                <Box>
                  <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                      <TableHead>
                        <StyledTableRow>
                          <StyledTableCell align="center" width="8%">
                            No.
                          </StyledTableCell>
                          <StyledTableCell align="left">Keterangan</StyledTableCell>
                          <StyledTableCell align="center" width="15%">
                            Qty
                          </StyledTableCell>
                          <StyledTableCell align="center" width="15%">
                            Biaya
                          </StyledTableCell>
                          <StyledTableCell align="center" width="15%">
                            Subtotal
                          </StyledTableCell>
                        </StyledTableRow>
                      </TableHead>
                      <TableBody>
                        {invoice_detail.invoice_detail ? invoice_detail.invoice_detail.map((row, index) => (
                          <TableRow>
                            <TableCell align="center">{index+1}</TableCell>
                            <TableCell align="left">{row.note}</TableCell>                      
                            <TableCell align="center">{row.qty}</TableCell>
                            <TableCell align="center">{currencyFormat(row.unit_price)}</TableCell>
                            <TableCell align="center">{currencyFormat(row.price)}</TableCell>
                          </TableRow>
                        )) : null}
                      </TableBody>
                      {/* <TableFooter>
                        <StyledTableRow>
                          <StyledTableCell component="th" scope="row" align="left" colspan={4}>
                            Total
                          </StyledTableCell>
                          <StyledTableCell component="th" scope="row" align="center">
                            Rp. 845000
                          </StyledTableCell>
                        </StyledTableRow>
                      </TableFooter> */}
                    </Table>
                  </TableContainer>
                </Box>
                <Box className={classes.divInvoice}>
                  <TableContainer component={Paper}>
                    <Table size="small" className={classes.table} aria-label="simple table">
                      <TableHead>
                        <StyledTableRow>
                          <StyledTableCell align="center" width="8%">
                            No.
                          </StyledTableCell>
                          <StyledTableCell align="left">Keterangan</StyledTableCell>
                          <StyledTableCell align="center"  width={100}>Kategori</StyledTableCell>
                          <StyledTableCell align="center"  width={300}>Nominal Pembayaran</StyledTableCell>
                        </StyledTableRow>
                      </TableHead>
                      <TableBody>
                        {entrie_detail.map((row, index) => (
                          <TableRow>
                            <TableCell align="center">{index+1}</TableCell>
                            <TableCell align="left">
                              {row.age_category} - {row.category_name}
                              <br/>
                              <small>a/n : {row.full_name}</small>
                            </TableCell>
                            <TableCell align="center">{row.category_type}</TableCell>
                            <TableCell align="center">{currencyFormat(row.registration_fee)}</TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                </Box>
                <Box>
                  <p style={{ textAlign: "left", fontSize: 16}}>
                      Konfirmasi Pembayaran: <br />
                      Nama Pengirim : {invoice_detail.payment_confirm ? invoice_detail.payment_confirm.account_holder : "-"} <br />
                      Nomor Rekening : {invoice_detail.payment_confirm ? invoice_detail.payment_confirm.bank_account : "-"} <br />
                      Tgl Transfer : {invoice_detail.payment_confirm ? moment(invoice_detail.payment_confirm.transfer_date).format("DD-MM-YYYY") : "-"} <br />
                      Bukti Transfer : {invoice_detail.payment_confirm ? invoice_detail.payment_confirm.payment_file : "-"} <br />
                    </p>
                </Box>
          {/* </ReactWaterMark> */}
          
        </Paper>
        <Box display="flex" justifyContent="flex-start" mt={3}>
          {
            localStorage.user_access === "Admin" ? 
            <>
              <Button onClick={()=>setopenModalReceivePayment(true)} variant="contained" color="primary" className={classes.button} startIcon={<GetAppIcon />} style={{ marginRight: "10px" }}>
                Terima Pembayaran
              </Button> 
              {/* {
                invoice_detail.invoice_status === "" ? */}
                <Button onClick={()=>setopenModalReset(true)} variant="contained" color="primary" className={classes.button} startIcon={<GetAppIcon />} style={{ marginRight: "10px" }}>
                  Reset Pembayaran
                </Button> 
                {/* : null
              } */}
            </>
            
            :
            <Button onClick={()=>setopenModal(true)} variant="contained" color="primary" className={classes.button} startIcon={<GetAppIcon />} style={{ marginRight: "10px" }}>
              Konfirmasi Pembayaran
            </Button>
          }
          
          
          <Link to="/profile/riwayatpembayaran">
            <Button variant="contained" color="primary" className={classes.button}>
              Kembali
            </Button>
          </Link>
        </Box>
      </div>
    </Fragment>
  );
};

export default Invoice;
