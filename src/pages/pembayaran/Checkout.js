import React, {useState, useEffect, useContext} from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Box, Button, Popover, Typography, IconButton, TableFooter } from "@material-ui/core";

// Icon
import GetAppIcon from "@material-ui/icons/GetApp";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { Link } from "react-router-dom";
import { PageTitle } from "../../components";
import api from "../../services/api"
import Alert from '@material-ui/lab/Alert';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import { Toll } from "@material-ui/icons";
import _ from "lodash"
import { InvoiceContext } from "../../context/invoicecontext"
import { ContingentContext } from "../../context/contingentcontext";
import { EventContext } from "../../context/eventcontext";

const useStyles = makeStyles((theme) => ({
    table: {
      marginTop: "20px",
      minWidth: 700,
      "& th": {
        fontWeight: "600",
      },
    },
    button: {
      minWidth: "40px !important",
      paddingLeft: 0,
      paddingRight: 0,
    },
}));

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: "#039be5",
      color: theme.palette.common.white,
      fontSize: 16,
      fontFamily: "Poppins !important",
    },
    body: {
      fontSize: 14,
      fontFamily: "Poppins !important",
    },
  }))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
      "&:nth-of-type(odd)": {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);

export default function Checkout(props) {
    const classes = useStyles();
    const [paymentlist, setpaymentlist] = useState([])
    const [totalbiaya, settotalbiaya] = useState(0)
    const { getDataInvoice, postDataInvoice, invoicedata} = useContext(InvoiceContext)
    const { getDataContingent, contingent} = useContext(ContingentContext)
    const { getDataEvent, eventdata} = useContext(EventContext)
    const {contingent_id} = props.match.params;
    useEffect(() => {
        getDataInvoice();
        getDataContingent(contingent_id);
        getDataEvent();
        api.postCheckout(contingent_id).then(res=>{

            setpaymentlist(res.data)
            var total = 0;
            res.data.map(x=>{
                total = total + x.registration_fee
            })
            settotalbiaya(total)
        })
    }, [])

    function currencyFormat(num) {
        return 'Rp. ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
     }

     function sumInvoices(p, c) {
        return _.extend(p, {qty:p.qty + 1, price:p.price + c.registration_fee});
    };

    function makeid(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
     }
     const create_invoice = () =>{
        //var invoicelength = invoicedata.length;
        console.log('rand', makeid(8))
        var b = _(paymentlist)
            .groupBy('category_type')
            .map(function(b) {return b.reduce(sumInvoices, {note:b[0].category_type, qty:0, unit_price:b[0].registration_fee, price:0})})
            .valueOf();
        console.log('invoice', b)
        postDataInvoice(paymentlist, contingent, eventdata, b, totalbiaya).then(data=>{
            props.history.push('/app/invoice/'+data.invoice_number)
        })
     }
    return (
        <div>
            <PageTitle title="Checkout Pendaftaran" />
                <TableContainer component={Paper} style={{marginBottom:20}} className={classes.table}>
                    <Table size="small" aria-label="customized table">
                    <TableHead>
                        <TableRow>
                        <StyledTableCell align="center" width={10}>
                            No
                        </StyledTableCell>
                        <StyledTableCell >Keterangan</StyledTableCell>
                        <StyledTableCell align="center"  width={100}>Kategori</StyledTableCell>
                        <StyledTableCell align="center"  width={300}>Nominal Pembayaran</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {paymentlist.map((row, index) => (
                        <StyledTableRow>
                            <StyledTableCell align="center">{index+1}</StyledTableCell>
                            <StyledTableCell>{row.age_category} - {row.category_name}
                            <br/>
                            <small>a/n : {row.full_name}</small>
                            </StyledTableCell>
                            <StyledTableCell align="center">{row.category_type}</StyledTableCell>
                            <StyledTableCell align="center">{currencyFormat(row.registration_fee)}</StyledTableCell>
                            
                        </StyledTableRow>
                        ))}
                    </TableBody>
                    <TableFooter>
                        <StyledTableRow>
                            <StyledTableCell colSpan="3"  align="left">
                                <Typography variant="h6">Total</Typography>
                            </StyledTableCell>
                            <StyledTableCell align="center">
                                <Typography>{currencyFormat(totalbiaya)}</Typography>
                            </StyledTableCell>
                        </StyledTableRow>
                    </TableFooter>
                    </Table>
                </TableContainer>

                <Alert severity="warning">Periksa kembali daftar nama rincian pembayaran diatas dan lakukan pembayaran sebelum batas waktu pembayaran</Alert>
                
                <Button onClick={create_invoice}
                        variant="contained"
                        color="primary"
                        size="default"
                        style={{marginTop:15, marginLeft:10}}
                        startIcon={<PersonAddIcon />}
                    >
                        Lanjut ke Pembayaran
                </Button>
        
        </div>
    )
}
