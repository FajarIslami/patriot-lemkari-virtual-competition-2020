import React, {useState, useContext, useEffect} from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Box, Button, Popover, Typography, IconButton, Select, MenuItem, TextField } from "@material-ui/core";

// Icon
import GetAppIcon from "@material-ui/icons/GetApp";
import DeleteIcon from '@material-ui/icons/Delete';
import VisibilityIcon from "@material-ui/icons/Visibility";
import { Link } from "react-router-dom";
import { PageTitle } from "../../components";
import { InvoiceContext } from "../../context/invoicecontext"
import { ContingentContext } from "../../context/contingentcontext"
import Dialog from "../../components/Dialog/Dialog"
import moment from "moment"

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#039be5",
    color: theme.palette.common.white,
    fontSize: 16,
    fontFamily: "Poppins !important",
  },
  body: {
    fontSize: 14,
    fontFamily: "Poppins !important",
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

function createData(no, tanggal, noFaktur, nominalPembayaran, aksi) {
  return { no, tanggal, noFaktur, nominalPembayaran, aksi };
}

const rows = [createData(1, "01/01/2020", "FKT-004", "Rp. 1.000.000,00"), createData(2, "01/01/2020", "FKT-003", "Rp. 1.000.000,00"), createData(3, "01/01/2020", "FKT-002", "Rp. 1.000.000,00"), createData(4, "01/01/2020", "FKT-001", "Rp. 1.000.000,00")];

const useStyles = makeStyles((theme) => ({
  table: {
    marginTop: "20px",
    minWidth: 700,
    "& th": {
      fontWeight: "600",
    },
  },
  button: {
    minWidth: "40px !important",
    paddingLeft: 0,
    paddingRight: 0,
  },
}));
const DaftarPembayaran = () => {
  const classes = useStyles();
  const { getDataInvoice, invoicedata} = useContext(InvoiceContext)
  const { getAllDataContingent, contingentall} = useContext(ContingentContext)

  const loadData = async()=>{
    await getAllDataContingent()
    if(localStorage.user_access === "kontingen"){
      getDataInvoice(localStorage.contingent_id).then(data=>{
        console.log('data', data)
      })
      setpilihkontingen(localStorage.contingent_id)
    }
    
  }
  useEffect(() => {
    loadData()
  }, [])

  function currencyFormat(num) {
    return 'Rp. ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
 }

 const [pilihkontingen, setpilihkontingen] = useState("0")

  return (
    <>
      <PageTitle title="Riwayat Pembayaran" />

      <div style={{marginBottom:5}}>
        <table>
          <tr>
            <td>Cari Berdasarkan :</td>
            <td>
              <Select style={{marginRight:20}}
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                disabled={localStorage.user_access === "kontingen" ? true : false}
                value={pilihkontingen}
                onChange={(event)=>{
                  setpilihkontingen(event.target.value);
                  getDataInvoice(event.target.value)
                }}
              >
                <MenuItem value="0">Pilih Kontingen</MenuItem>
                {
                  contingentall.map(item=>{
                    return(
                      <MenuItem value={item._id}>{item.name}</MenuItem>
                    )
                  })
                }
              </Select>
              <TextField type="search" variant="standard"/>
            </td>
          </tr>
        </table>
      </div>

      <TableContainer component={Paper} className={classes.table}>
        <Table size="small" aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell align="center" width={20}>
                No
              </StyledTableCell>
              <StyledTableCell width={200}>Tanggal</StyledTableCell>
              <StyledTableCell align="left">Keterangan</StyledTableCell>
              <StyledTableCell width={200} align="center">Nominal Pembayaran</StyledTableCell>
              <StyledTableCell width={100} align="center">
                Status
              </StyledTableCell>
              <StyledTableCell width={100} align="center">
                Aksi
              </StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {invoicedata.map((row, index) => (
              <StyledTableRow>
                <StyledTableCell align="center">{index+1}</StyledTableCell>
                <StyledTableCell>{moment(row.invoice_date).format("DD-MM-YYYY HH:mm:ss")}</StyledTableCell>
                <StyledTableCell align="left">{row.invoice_number} - {row.contingent_name}
                </StyledTableCell>
                <StyledTableCell align="center">{currencyFormat(row.invoice_total)}</StyledTableCell>
                <StyledTableCell align="center">
                  { 
                    row.invoice_status === "Lunas" ? 
                    <Button size="small" variant="contained" color="primary"><Typography style={{fontSize:9}}>{row.invoice_status}</Typography></Button>
                    :
                    row.invoice_status !== "" ? 
                    <Button size="small" variant="contained" style={{background:'yellowgreen'}}><Typography style={{fontSize:9}}>{row.invoice_status}</Typography></Button>
                    :
                    <Button size="small" variant="contained" color="secondary"><Typography style={{fontSize:8}}>Belum Bayar</Typography></Button>
                  }
                </StyledTableCell>
                <StyledTableCell align="center">
                  <Link to={"/app/invoice/"+row.invoice_number}>
                    <Button variant="outlined" color="primary" className={classes.button}>
                      <VisibilityIcon fontSize="small" color="primary" />
                    </Button>
                  </Link>
                  
                </StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default DaftarPembayaran;
