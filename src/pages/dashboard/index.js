import React, { useEffect, useContext, useState, Fragment } from "react";
import { Card, CardContent, Typography, Grid, Button } from "@material-ui/core";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import PersonIcon  from "@material-ui/icons/Person";
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import FlagIcon from '@material-ui/icons/Flag';

import cx from "classnames";
// importing styles
import styles from "./index.module.css";
import { UserContext } from "../../context/usercontext";
import { PageTitle } from "../../components";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Alert from '@material-ui/lab/Alert';
import { ParticipantContext } from "../../context/participantcontext"
import { InvoiceContext } from "../../context/invoicecontext"
import { ContingentContext } from "../../context/contingentcontext"
import GetAppIcon from "@material-ui/icons/GetApp";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { Link } from "react-router-dom";
import moment from "moment"

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#039be5",
    color: theme.palette.common.white,
    fontSize: 16,
    fontFamily: "Poppins !important",
  },
  body: {
    fontSize: 14,
    fontFamily: "Poppins !important",
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles((theme) => ({
  table: {
    marginTop: "20px",
    minWidth: 700,
    "& th": {
      fontWeight: "600",
    },
  },
  button: {
    minWidth: "40px !important",
    paddingLeft: 0,
    paddingRight: 0,
  },
}));

export default function Dashboard() {
  const classes = useStyles();
  const { getDataAthlete, participant} = useContext(ParticipantContext)
  const { getAllDataContingent, contingentall} = useContext(ContingentContext)
  const { getDataInvoice, invoicedata} = useContext(InvoiceContext)
  const [jumlahopentournament, setjumlahopentournament] = useState(0)
  const [jumlahfestival, setjumlahfestival] = useState(0)
  const userlogin = JSON.parse(localStorage.user)
  const [jumlahpeserta, setjumlahpeserta] = useState(0)
  const [jumlahkontingen, setjumlahkontingen] = useState(0)
  const loadData = async()=>{
    getAllDataContingent().then(async (data)=>{
      var entrie = 0;
      var open = 0;
      var festival = 0;
      var peserta =0;
      var kontingen = 0;
      await data.map((item)=>{
        peserta = peserta+item.participant
        kontingen = kontingen + 1
        item.entries.map(row=>{
            entrie = entrie+row.count
            if(row._id === "OPEN")
            {
                open = open + row.count
            }
            if(row._id === "FESTIVAL")
            {
                festival = festival + row.count
            }
          })
      })
      setjumlahpeserta(peserta)
      setjumlahfestival(festival)
      setjumlahopentournament(open)
      setjumlahkontingen(kontingen)
    })

    getDataInvoice()
  }
  useEffect(() => {
    loadData()
  }, [])

  function currencyFormat(num) {
    return 'Rp. ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
 }
  return (
    <Fragment >
       <PageTitle title={"Hi, "+userlogin.name} />
        <Grid container spacing={2} style={{marginTop:20}}>
          <Grid item xs={12} md={3} >
            <Card>
              <CardContent>
                <Typography color="textSecondary" gutterBottom>
                  Total Peserta
                </Typography>
                <Typography variant="h5" component="h2">
                  {jumlahpeserta}
                </Typography>
              </CardContent>
            </Card>
          </Grid>

          <Grid item xs={12} md={3} >
          
            <Card>
              <CardContent>
                <Typography color="textSecondary" gutterBottom>
                  Total Open Tournament
                </Typography>
                <Typography variant="h5" component="h2">
                  {jumlahopentournament}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
          
          <Grid item xs={12} md={3} >
            <Card>
              <CardContent>
                <Typography color="textSecondary" gutterBottom>
                  Total Festival
                </Typography>
                <Typography variant="h5" component="h2">
                  {jumlahfestival}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
          
          <Grid item xs={12} md={3} >
            <Card>
              <CardContent>
                <Typography color="textSecondary" gutterBottom>
                  Total Kontingen
                </Typography>
                <Typography variant="h5" component="h2">
                  {jumlahkontingen}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        </Grid>

        <Alert severity="warning" style={{marginTop:20}}>Pastikan Anda membayar pendaftaran sebelum jatuh tempo untuk memastikan data anda telah terdaftar didalam sistem karena Quota Peserta Terbatas. Batas pembayaran pendaftaran adalah 2 hari setelah pemasukan data.</Alert>

        <Typography variant="h6" style={{marginTop:20}}>
          Tagihan Pembayaran
        </Typography>
        <Table size="small" aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell align="center" width={20}>
                No
              </StyledTableCell>
              <StyledTableCell width={200}>Tanggal</StyledTableCell>
              <StyledTableCell align="left">Keterangan</StyledTableCell>
              <StyledTableCell width={200} align="center">Nominal Pembayaran</StyledTableCell>
              <StyledTableCell width={100} align="center">
                Status
              </StyledTableCell>
              <StyledTableCell width={100} align="center">
                Rincian
              </StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              localStorage.user_access === "Admin" ?
              invoicedata.filter(x=>x.invoice_status !== "Lunas").map((row, index) => (
                <StyledTableRow>
                  <StyledTableCell align="center">{index+1}</StyledTableCell>
                  <StyledTableCell>{moment(row.invoice_date).format("DD-MM-YYYY HH:mm:ss")}</StyledTableCell>
                  <StyledTableCell align="left">{row.invoice_number} - {row.contingent_name}
                  </StyledTableCell>
                  <StyledTableCell align="center">{currencyFormat(row.invoice_total)}</StyledTableCell>
                  <StyledTableCell align="center">
                    { 
                      row.invoice_status === "Lunas" ? 
                      <Button size="small" variant="contained" color="primary"><Typography style={{fontSize:9}}>{row.invoice_status}</Typography></Button>
                      :
                      row.invoice_status !== "" ? 
                      <Button size="small" variant="contained" style={{background:'yellowgreen'}}><Typography style={{fontSize:9}}>{row.invoice_status}</Typography></Button>
                      :
                      <Button size="small" variant="contained" color="secondary"><Typography style={{fontSize:8}}>Belum Bayar</Typography></Button>
                    }
                  </StyledTableCell>
                  <StyledTableCell align="center">
                    <Link to={"/app/invoice/"+row.invoice_number}>
                      <Button variant="outlined" color="primary" className={classes.button}>
                        <VisibilityIcon fontSize="small" color="primary" />
                      </Button>
                    </Link>
                    &nbsp; &nbsp;
                    <Button variant="outlined" color="primary" href="#" className={classes.button}>
                      <GetAppIcon color="primary" fontSize="small" />
                    </Button>
                  </StyledTableCell>
                </StyledTableRow>
              )) :
              invoicedata.filter(x=>x.invoice_status !== "Lunas" && x.contingent_id === localStorage.contingent_id).map((row, index) => (
                <StyledTableRow>
                  <StyledTableCell align="center">{index+1}</StyledTableCell>
                  <StyledTableCell>{moment(row.invoice_date).format("DD-MM-YYYY HH:mm:ss")}</StyledTableCell>
                  <StyledTableCell align="left">{row.invoice_number} - {row.contingent_name}
                  </StyledTableCell>
                  <StyledTableCell align="center">{currencyFormat(row.invoice_total)}</StyledTableCell>
                  <StyledTableCell align="center">
                    { 
                      row.invoice_status === "Lunas" ? 
                      <Button size="small" variant="contained" color="primary"><Typography style={{fontSize:9}}>{row.invoice_status}</Typography></Button>
                      :
                      row.invoice_status !== "" ? 
                      <Button size="small" variant="contained" style={{background:'yellowgreen'}}><Typography style={{fontSize:9}}>{row.invoice_status}</Typography></Button>
                      :
                      <Button size="small" variant="contained" color="secondary"><Typography style={{fontSize:8}}>Belum Bayar</Typography></Button>
                    }
                  </StyledTableCell>
                  <StyledTableCell align="center">
                    <Link to={"/app/invoice/"+row.invoice_number}>
                      <Button variant="outlined" color="primary" className={classes.button}>
                        <VisibilityIcon fontSize="small" color="primary" />
                      </Button>
                    </Link>
                    &nbsp; &nbsp;
                    <Button variant="outlined" color="primary" href="#" className={classes.button}>
                      <GetAppIcon color="primary" fontSize="small" />
                    </Button>
                  </StyledTableCell>
                </StyledTableRow>
              ))
            }
            
          </TableBody>
        </Table>
    </Fragment>
  );
}
