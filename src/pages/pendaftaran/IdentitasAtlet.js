import React, { Fragment, useState, useContext, useEffect, useRef } from "react";
//style
import { makeStyles } from "@material-ui/core/styles";
//component
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import Avatar from "@material-ui/core/Avatar";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Fab from "@material-ui/core/Fab";
import Grid from "@material-ui/core/Grid";
import MenuItem from "@material-ui/core/MenuItem";
import Paper from "@material-ui/core/Paper";
import SaveIcon from "@material-ui/icons/Save";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import {FormControl, InputLabel, FormHelperText, Input, Table, TableHead, TableRow, TableCell, TableBody, Checkbox} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { EventContext } from "../../context/eventcontext";
import { ParticipantContext } from "../../context/participantcontext"
import moment from "moment";
import api from "../../services/api";

const useStyles = makeStyles((theme) => ({
  paperTitle: {
    display: "flex",
    flexWrap: "wrap",
    backgroundColor: "#bbdefb",
    color: "#0d47a1",
    width: "100%",
    height: theme.spacing(9),
    justifyContent: 'center'
  },
  paperForm:{
    display: 'flex',
    flexWrap: 'wrap',
    width: 'auto',
    height: 'auto',
    marginTop: 30,
  },
  boxButton: {
      margin:30
  },
  inputForm:{
      padding: theme.spacing(2),
      marginTop: 20,
      marginLeft: 15,
      marginRight: 15,
      marginBottom: 15,
  },
  avatar: {
      width: theme.spacing(15),
      height: theme.spacing(15),
  },
}));

const jenisKelamin = [
  {
    value: "L",
    label: "Laki-Laki",
  },
  {
    value: "P",
    label: "Perempuan",
  },
];

const kategoriPertandingan = [
  {
    value: "Open Perorangan",
    label: "Open Perorangan",
  },
  {
    value: "Festival",
    label: "Festival",
  },
];

const kategoriSabuk = [
  {
    value: "Putih",
    label: "Putih",
  },
  {
    value: "Kuning",
    label: "Kuning",
  },
  {
    value: "Hijau",
    label: "Hijau",
  },
  {
    value: "Biru Muda",
    label: "Biru Muda",
  },
  {
    value: "Biru Tua",
    label: "Biru Tua",
  },
  {
    value: "Ungu",
    label: "Ungu",
  },
  {
    value: "Cokelat",
    label: "Cokelat",
  },
];

const kategoriJenjangPendidikan = [
  {
    value: "SD/MI",
    label: "SD/MI",
  },
  {
    value: "SMP/MTs",
    label: "SMP/MTs",
  },
  {
    value: "SMA/MA",
    label: "SMA/MA",
  },
];

const IdentitasAtlet = (props) => {
  const classes = useStyles();
  const [jenKel, setjenKel] = useState("");
  const [tgllahir, settgllahir] = useState("")
  const [nama, setnama] = useState("")
  const [image, setImage] = useState("");
  const imageHandler = (e) => {
    const reader = new FileReader();
    reader.onload = () => {
      if (reader.readyState === 2) {
        setImage(reader.result);
      }
    };
    reader.readAsDataURL(e.target.files[0]);
  };

  const pilihJenisKelamin = (event) => {
    setjenKel(event.target.value);
  };

  var { getDataEvent, eventdata } = useContext(EventContext);
  const { getDetailAthlete, postDataParticipant, putDataParticipant} = useContext(ParticipantContext)
  const [event_categories, setevent_categories] = useState([])
  const [usia, setusia] = useState(0)
  const [foto, setfoto] = useState()
  const [tempatlahir, settempatlahir] = useState("")
  const [participant_categories, setparticipant_categories] = useState([])
  const [categories, setcategories] = useState([])
  const action = props.match.params.action
  const loadData = async() =>{
    if(props.match.params.action === "edit"){
      await getDetailAthlete(props.match.params.id).then(data=>{
        console.log('datadetail', data)
        setnama(data.full_name);
        setjenKel(data.gender);
        settempatlahir(data.place_of_birth)
        settgllahir( moment(data.date_of_birth).format("YYYY-MM-DD"))
        var tgl = new Date(data.date_of_birth).getFullYear()
        var sekarang = new Date().getFullYear()
        var hitung_usia = sekarang-tgl
        setusia(hitung_usia)
        setcategories(data.categories)

        getDataEvent().then(dataevent=>{
          var datacategory = dataevent.event_categories.map(x=>{
            var datacheck = data.categories.filter(y=>y._id===x._id)            
            return({...x, payment_status: datacheck.length > 0 ?datacheck[0].payment_status:null, invoice_number:datacheck.length > 0 ?datacheck[0].invoice_number:"-", checked : datacheck.length>0 ? true : false})
          })
          var datastringcategory = datacategory.filter(x=>x.checked === true).map(y=>y._id)
          console.log('datastringcategory', datastringcategory)
          setparticipant_categories(datastringcategory)
          setevent_categories(datacategory)
        })
      })
    }    
    else{
      getDataEvent().then(data=>{
        var datacategory = data.event_categories.map(x=>{return({...x, checked : false})})        
        console.log('datacategory', datacategory)
        setevent_categories(data.event_categories)
      })
    }
    
  }
  useEffect(() => {
    loadData()
  }, [])

  const simpanpendaftaran = () =>{
    
    var datachecklist = []
    event_categories.map(x=>{
      var checklist = participant_categories.filter(y=>y === x._id).length
      if(checklist){
        datachecklist = [...datachecklist, x]
      }
    })

    var data = {
      full_name : nama,
      gender : jenKel,
      date_of_birth : tgllahir,
      place_of_birth : tempatlahir,
      contingent_id : props.match.params.contingent_id,
      event_id : eventdata._id,
      participant_type: 'Athlete',
      event_id : eventdata._id,
      categories : datachecklist
    }
    if(action === "add"){
      postDataParticipant(data).then(()=>{
        props.history.goBack()
      })

    }
    else{
      putDataParticipant(props.match.params.id, data).then(()=>{
        props.history.goBack()
      })
    }
    

  }

  const ceklistnomor = (event) =>{
    console.log(event.target.checked)
    console.log(event.target.value)
    if(event.target.checked){
      var data = [...participant_categories, event.target.value ]
      setparticipant_categories(data)
      var datacategory = event_categories.map(x=>
        {
          if(x._id === event.target.value){
            return(
              {
                ...x, checked : true
              }
            )
          }
          else{
            return x
          }
        })
      
      console.log('datacategory', datacategory)
      setevent_categories(datacategory)
      console.log(data)
    }
    else{
      var data = participant_categories.filter(x=>x !== event.target.value)
      var datacategory = event_categories.map(x=>
        {
          if(x._id === event.target.value){
            return(
              {
                ...x, checked : false
              }
            )
          }
          else{
            return x
          }
        })
      console.log('datacategory', datacategory)
      setevent_categories(datacategory)
      setparticipant_categories(data)
      console.log(data)
    }
  }

  const fotoRef = useRef()

  return (
    <Fragment>
      <div style={{marginTop:10}}>
        <Alert severity="warning">Pastikan Anda setelah mengisi formulir dibawah ini. Lakukan Check-out pembayaran untuk memastikan data anda telah terdaftar didalam sistem. Batas pembayaran pendaftaran adalah 2 hari setelah pemasukan data.</Alert>
        
        <div style={{width:'100%', backgroundColor:'#CFCFCF', marginTop:10}}>
          <Typography style={{padding:15}}>
            1. Isian Identitas Peserta
          </Typography>
          
        </div>
        <div style={{border:'1px solid #CFCFCF', width:'100%'}}>
          <Grid container spacing={2} style={{padding:20}}>
            <Grid item md={12} xs={12}>
              <Typography>Nama Lengkap</Typography>
              <TextField id="namaLengkap" value={nama} onChange={(event)=>setnama(event.target.value)} variant="outlined" fullWidth required />
            </Grid>
            <Grid item md={12} xs={12}>
              <Typography>Jenis Kelamin</Typography>
              <TextField id="jenis_kelamin" select value={jenKel} onChange={pilihJenisKelamin} variant="outlined" fullWidth required>
                {jenisKelamin.map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item md={12} xs={12}>
              <Typography>Tempat Lahir</Typography>
              <TextField id="tempatlahir" value={tempatlahir} onChange={(event)=>settempatlahir(event.target.value)} variant="outlined" fullWidth required />
            </Grid>
            <Grid item md={12} xs={12}>
              <Typography style={{marginBottom:10}}>Tanggal Lahir</Typography>
              <TextField
                id="tanggalLahir"
                type="date"
                value={tgllahir}
                onChange={(event)=> {
                  var tgl = new Date(event.target.value).getFullYear()
                  var sekarang = new Date().getFullYear()
                  var hitung_usia = sekarang-tgl
                  setusia(hitung_usia)
                  settgllahir(event.target.value);
                  
                }}
                defaultValue={new Date()}
                InputLabelProps={{
                  shrink: true,
                }}
                variant="outlined"
                fullWidth
                required
              />
            </Grid>
            
            {/* <Grid item md={12} xs={12}>
              <Typography>Foto Peserta</Typography>
              <input type="file" accept="image/*" onChange={e=>{
                  const [file] = e.target.files;
                  if (file) {
                      const reader = new FileReader();
                      const {current} = fotoRef;
                      current.file = file;
                      reader.onload = (e) => {
                          current.src = e.target.result;
                      }
                      reader.readAsDataURL(file);
                  }
                }}/>
              <Box>
                <img ref={fotoRef} style={{width:150, height:150}}/>
              </Box>
            </Grid> */}
          </Grid>
          
        </div>
        
        <div style={{width:'100%', backgroundColor:'#CFCFCF',  marginTop:10}}>
          <Typography style={{padding:15}}>
            2. Nomor Pertandingan
          </Typography>
        </div>
        <div style={{border:'1px solid #CFCFCF', width:'100%', marginBottom:20}}>
          <Table size={"small"}>
            <TableHead>
              <TableRow>
                <TableCell style={{width:10}}>Pilih</TableCell>
                <TableCell>Nomor Pertandingan</TableCell>
                <TableCell style={{width:200,textAlign:'center'}}>Usia</TableCell>
                <TableCell style={{width:200,textAlign:'center'}}>Tipe</TableCell>
                <TableCell style={{width:100,textAlign:'center'}}>Status</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {
                event_categories.filter(x=>x.category_gender === jenKel && x.min_age <= usia  && x.max_age >= usia).map((item,index)=>{
                  return(
                    <TableRow>
                      <TableCell style={{width:10}}>
                        <Checkbox disabled={item.invoice_number? item.invoice_number !== "-"? true : false : false} value={item._id} checked={item.checked} onChange={ceklistnomor} />
                      </TableCell>
                      <TableCell>{item.category_name}</TableCell>
                      <TableCell style={{textAlign:'center'}}>{item.age_category}</TableCell>
                      <TableCell style={{textAlign:'center'}}>{item.category_type}</TableCell>
                      <TableCell style={{textAlign:'center'}}>{item.invoice_number}</TableCell>
                    </TableRow>
                  )
                })
              }
            </TableBody>
          </Table>
        </div>
        <Button onClick={simpanpendaftaran} variant="contained" color="primary" startIcon={<SaveIcon />}>
          Simpan
        </Button>
      </div>
    </Fragment>
  );
};

export default IdentitasAtlet;
