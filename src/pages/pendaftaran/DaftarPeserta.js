import React, { Component, Fragment, useState, useContext, useEffect } from "react";
// import Material-Ui
import {
  Grid, Typography, IconButton, Box, TextField, Button,
  Paper, Table, TableBody, TableCell, TableContainer, TableHead,
  TablePagination, TableRow, Select, MenuItem, CircularProgress
} from '@material-ui/core';
import { Link } from "react-router-dom";
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import { makeStyles } from '@material-ui/core/styles';
import { PageTitle } from "../../components";
import Chip from '@material-ui/core/Chip';
// importing styles
import styles from "./index.css";
import { ParticipantContext } from "../../context/participantcontext"
import { ContingentContext } from "../../context/contingentcontext"
import Dialog from '../../components/Dialog/Dialog'
import moment from "moment"
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => ({
  function: {
    display: "flex",
    justifyContent: "space-between",
    margin: "30px 0px"
  }
}))

const useDesigns = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },

});

export default function DaftarPeserta(props) {
  const classes = useStyles();
  const classDesign = useDesigns();
  const { getDataAthlete, participant, deleteDataParticipant, loadingparticipant} = useContext(ParticipantContext)
  const { getAllDataContingent, contingentall} = useContext(ContingentContext)

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const loadData = async()=>{
    await getAllDataContingent()
    if(localStorage.user_access === "kontingen"){
      getDataAthlete(localStorage.contingent_id).then(data=>{
        console.log('data', data)
      })
      setpilihkontingen(localStorage.contingent_id);
    }
  }

  useEffect(() => {
    loadData()
  }, [])

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const [pilihkontingen, setpilihkontingen] = useState("0")
  const [openModal, setopenModal] = useState(false)
  const [participant_id, setparticipant_id] = useState('')
  const [participant_name, setparticipant_name] = useState('')
  const [contingent_id, setcontingent_id] = useState('')
  const [invoice_number, setinvoice_number] = useState('')
  const handleClose = () =>{
    setopenModal(false)
  }

  const prosesdeleteDataParticipant = ()=>{
    deleteDataParticipant(participant_id, contingent_id, invoice_number).then(data=>{
      if(data){
        loadData()
        setopenModal(false)
      }
    })
  }
  
  return (
    <Fragment>
      <PageTitle title="Daftar Nama Peserta" />

      <Dialog
        open={openModal}
        close={handleClose}
        title={<Typography style={{color:'#bf272b'}}>Hapus Data Peserta</Typography>}
        content={
            <>
              <Typography>Apakah Anda yakin menghapus data : {participant_name}</Typography>
              <Typography style={{fontSize:11}}>Menghapus data peserta secara otomatis akan mereset data pembayaran anda. <br/>Lanjutkan Checkout kembali setelah menekan tombol Hapus dibawah ini </Typography>
            </>
        }
        cancel={handleClose}
        confirm={ prosesdeleteDataParticipant}
        valueConfirm={"Ya, Hapus"}
        valueCancel={"Tutup"}
        colorButtonConfirm={"#bf272b"}
    />
      <div style={{marginBottom:5}}>
        <table>
          <tr>
            <td>Cari Berdasarkan :</td>
            <td>
              <Select style={{marginRight:20}}
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                disabled={localStorage.user_access === "kontingen" ? true : false}
                value={pilihkontingen}
                onChange={(event)=>{
                  setpilihkontingen(event.target.value);
                  getDataAthlete(event.target.value)
                }}
              >
                <MenuItem value="0">Pilih Kontingen</MenuItem>
                {
                  contingentall.map(item=>{
                    return(
                      <MenuItem value={item._id}>{item.name}</MenuItem>
                    )
                  })
                }
              </Select>
            <TextField type="search" variant="standard"/>
            </td>
          </tr>
        </table>
      </div>
      <Alert severity="warning">Peserta yang telah didaftarkan paling lambat 2 hari saat tgl pendaftaran diharuskan untuk menyelesaikan pembayaran. Apabila peserta tidak membayar sampai batas waktu tersebut, sistem akan melakukan penghapus data secara otomatis.</Alert>
      <Paper style={{marginTop:10}} className={classDesign.root}>

      
      <Table size="small" aria-label="sticky table">
        <TableHead>
          <TableRow>
            <TableCell style={{width:10, textAlign:'center'}}> No</TableCell>
            <TableCell> Nama</TableCell>
            <TableCell style={{width:100, textAlign:'center'}}> Jenis Kelamin</TableCell>
            <TableCell style={{width:100, textAlign:'center'}}> Tgl Lahir</TableCell>
            <TableCell style={{width:500}}> Nomor Pertandingan</TableCell>
            <TableCell style={{width:100, textAlign:'center'}}> Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            loadingparticipant ? 
            <TableRow>
              <TableCell colSpan="6">
                  <center>
                    <CircularProgress />
                    <p>Sedang memuat...</p>
                  </center>
              </TableCell>
            </TableRow> :

            participant.map((row, index) => {
              return (
                <TableRow hover tabIndex={-1}>
                  <TableCell>{index+1}</TableCell>
                  <TableCell> {row.full_name}</TableCell>
                  <TableCell style={{textAlign:'center'}}> {row.gender}</TableCell>
                  <TableCell style={{textAlign:'center'}}> {moment(row.date_of_birth).format("DD-MM-YYYY")}</TableCell>
                  <TableCell> 
                      <Table size="small">
                        {
                          
                          row.categories.map((itemnomor, indexnomor)=>{
                          return(
                            <TableRow>
                              <TableCell style={{width:10}}>
                                {indexnomor+1}
                              </TableCell>
                              <TableCell>
                                {itemnomor.age_category+ " - "+itemnomor.category_name}
                              </TableCell>
                              <TableCell style={{width:100, textAlign:'right'}}>
                                {itemnomor.payment_status ? 
                                <Button size="small" variant="contained" color="primary">LUNAS</Button>
                                :
                                <Button size="small" variant="contained" color="secondary"><Typography style={{fontSize:8}}>BELUM BAYAR</Typography></Button>
                                }
                                
                              </TableCell>
                            </TableRow>
                          )
                        })}
                        
                      </Table>
                      

                  </TableCell>
                  <TableCell style={{textAlign:'center'}}> 
                    <Button onClick={()=>props.history.push('/app/formpendaftaran/'+row._id+'/'+pilihkontingen+'/edit')} style={{marginBottom:5}} size="small" variant="contained" color="primary">Ubah</Button>
                    <Button size="small" variant="contained" color="secondary" onClick={async()=>{
                      setopenModal(true);
                      setparticipant_id(row._id)
                      setcontingent_id(row.contingent_id)
                      setparticipant_name(row.full_name)
                      var invoice = ""
                      await row.categories.map(async(itemnomor)=>{
                        invoice = await itemnomor.invoice_number
                      })
                      setinvoice_number(invoice)
                    }}>Hapus</Button>
                  </TableCell>
                </TableRow>
              )
            })
          }
        </TableBody>
      </Table>

        
      </Paper>
      {pilihkontingen !== "0" ? 
      <>
        <Button onClick={()=>props.history.push('/app/formpendaftaran/0/'+pilihkontingen+'/add')}
            variant="contained"
            color="primary"
            size="default"
            style={{marginTop:15}}
            startIcon={<PersonAddIcon />}
          >
            Tambah Peserta
      </Button>
      <Button onClick={()=>props.history.push('/app/checkout/'+pilihkontingen)}
            variant="contained"
            color="primary"
            size="default"
            style={{marginTop:15, marginLeft:10}}
            startIcon={<PersonAddIcon />}
          >
            Checkout Pendaftaran
      </Button>
      </> : null
      }
      
    </Fragment>
  );
}