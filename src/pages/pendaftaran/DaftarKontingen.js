import React, {useState, useContext, useEffect} from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableFooter from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Box, Button, Popover, Typography, IconButton } from "@material-ui/core";

// Icon
import GetAppIcon from "@material-ui/icons/GetApp";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { Link } from "react-router-dom";
import { PageTitle } from "../../components";
import { ContingentContext } from "../../context/contingentcontext"
import moment from "moment"
import Dialog from '../../components/Dialog/Dialog'
import { UserContext } from "../../context/usercontext";
import _ from "lodash";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#039be5",
    color: theme.palette.common.white,
    fontSize: 16,
    fontFamily: "Poppins !important",
  },
  body: {
    fontSize: 14,
    fontFamily: "Poppins !important",
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles((theme) => ({
    table: {
      marginTop: "20px",
      minWidth: 700,
      "& th": {
        fontWeight: "600",
        fontSize:13
      },
    },
    button: {
      minWidth: "40px !important",
      paddingLeft: 0,
      paddingRight: 0,
    },
}));
  
export default function DaftarKontingen() {
    const classes = useStyles();
    const { getAllDataContingent, deleteDataContingent, contingentall} = useContext(ContingentContext)
    const [contingentdata, setcontingentdata] = useState({})
    const { getDataUser, user} = useContext(UserContext)
    const [jumlahentries, setjumlahentries] = useState(0)
    const [jumlahopen, setjumlahopen] = useState(0)
    const [jumlahfestival, setjumlahfestival] = useState(0)
    const [jumlahparticipant, setjumlahparticipant] = useState(0)

    const getdata = async()=>{
        getDataUser();
        var participant = 0;
        var entrie = 0;
        var open = 0;
        var festival = 0;
        await getAllDataContingent().then(data=>{   
            data.map(item=>{
                participant = participant+ item.participant
                if(item.entries.length>0){
                    item.entries.map(row=>{
                        
                        entrie = entrie+row.count
                        if(row._id === "OPEN")
                        {
                            open = open + row.count
                        }
                        if(row._id === "FESTIVAL")
                        {
                            festival = festival + row.count
                        }
                    })
                }
            })
            
        })
        setjumlahparticipant(participant)
        setjumlahentries(entrie)
        setjumlahopen(open)
        setjumlahfestival(festival)
    }
    useEffect(() => {
        getdata();
    }, [])

    const [openModal, setopenModal] = useState(false)
    const handleClose = ()=>{
        setopenModal(false)
    }
    return (
        <>
            <Dialog
                open={openModal}
                close={handleClose}
                title={<Typography style={{color:'#bf272b'}}>Info User</Typography>}
                content={
                    <>
                        {
                            user? user.filter(x=>x.contingent_id === contingentdata._id).map(item=>{
                                return(
                                    <>
                                        <Typography>Nama Kontingen : {contingentdata.name}</Typography>
                                        <Typography>Nama Manager : {item.name}</Typography>
                                        <Typography>Telp : {item.telp}</Typography>
                                        <Typography>Email : {item.email}</Typography>
                                        <Typography>Password : {item.password}</Typography>
                                    </>
                                )
                            }): null
                        }
                        
                    </>
                }
                cancel={handleClose}
                // confirm={ receivepayment}
                // valueConfirm={"Ya, Terima Pembayaran"}
                valueCancel={"Tutup"}
                colorButtonConfirm={"#bf272b"}
            />
            <PageTitle title="Daftar Kontingen" />
                <TableContainer component={Paper} className={classes.table}>
                    <Table size="small" aria-label="customized table">
                    <TableHead>
                        <TableRow>
                        <StyledTableCell align="center" width={20}>
                            No
                        </StyledTableCell>
                        <StyledTableCell>Nama Kontingen</StyledTableCell>
                        <StyledTableCell width={80} align="center">Jumlah Peserta</StyledTableCell>
                        <StyledTableCell width={80} align="center">Jumlah Entries</StyledTableCell>
                        <StyledTableCell width={80} align="center">Jumlah Open</StyledTableCell>
                        <StyledTableCell width={80} align="center">
                            Jumlah Festival
                        </StyledTableCell>
                        <StyledTableCell width={100} align="center">
                            Lihat
                        </StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            _.orderBy(contingentall, ['participant'], ['desc']).map((item, index)=>{
                                var entrie = 0;
                                var open = 0;
                                var festival = 0;
                                item.entries.map(row=>{
                                    entrie = entrie+row.count
                                    if(row._id === "OPEN")
                                    {
                                        open = open + row.count
                                    }
                                    if(row._id === "FESTIVAL")
                                    {
                                        festival = festival + row.count
                                    }
                                })

                                var userdetail = user ? user.filter(x=>x.contingent_id === item._id).map(y=>y): []
                                return(
                                    <TableRow>
                                        <StyledTableCell align="center" width={20}>{index+1}</StyledTableCell>
                                        <StyledTableCell><strong>{item.name}</strong>
                                            {
                                                userdetail.map(itemuser=>{
                                                    return(
                                                        <>
                                                            <br/>
                                                            <small>Manager : {itemuser.name} / {itemuser.telp}</small><br/>
                                                            <small>Alamat : {itemuser.address}</small>
                                                        </>
                                                    )
                                                })
                                            }
                                        </StyledTableCell>
                                        <StyledTableCell align="center">{item.participant}
                                        </StyledTableCell>
                                        <StyledTableCell align="center">{entrie}</StyledTableCell>
                                        <StyledTableCell align="center">{open}</StyledTableCell>
                                        <StyledTableCell align="center">{festival}</StyledTableCell>
                                        <StyledTableCell align="center">
                                            <Button variant="contained" color="primary" onClick={()=>{setopenModal(true); setcontingentdata(item)}}>Lihat</Button>
                                            {
                                                item.participant == 0 ? 
                                                <Button variant="contained" color="secondary" onClick={()=>deleteDataContingent(item._id)}>Hapus</Button>:
                                                null
                                            }
                                            
                                        </StyledTableCell>
                                    </TableRow>
                                )
                            })
                        }
                        <TableRow>
                            <StyledTableCell align="left" colSpan="2">Total</StyledTableCell>
                            <StyledTableCell width={80} align="center">{jumlahparticipant}</StyledTableCell>
                            <StyledTableCell width={80} align="center">{jumlahentries}</StyledTableCell>
                            <StyledTableCell width={80} align="center">{jumlahopen}</StyledTableCell>
                            <StyledTableCell width={80} align="center">{jumlahfestival}</StyledTableCell>
                            <StyledTableCell width={100} align="center">
                            </StyledTableCell>
                        </TableRow>
                    </TableBody>
                        
                    </Table>
                </TableContainer>
        </>
    )
}
