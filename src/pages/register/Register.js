import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import { Container, FormControl, RadioGroup, Grid, Button, FormControlLabel, 
  Checkbox, TextField, FormHelperText, InputLabel, OutlinedInput, InputAdornment, IconButton, Typography } from "@material-ui/core";
import {VisibilityOff, Visibility} from "@material-ui/icons";
import { InputTextRegis, InputImageRegis, InputPassRegis } from "../../components";
import RadioButtons from "../../components/RadioButtonRegis";
import { Link } from "react-router-dom";
import auth from "../../services/auth"
import { Alert } from "@material-ui/lab";
import Dialog from '../../components/Dialog/Dialog'

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
    minWidth: 275,
    borderRadius: "50px",
    padding: theme.spacing(4, 8, 4, 4),
    fontFamily: "Poppins !important",
    "& span": {
      [theme.breakpoints.down("sm")]: {
        textAlign: "center",
      },
    },
    [theme.breakpoints.down("sm")]: {
      margin: "10px 0px",
      padding: 10,
    },
  },
  title: {
    fontWeight: "600",
    fontSize: "1.5rem",
  },
  subtitle: {
    fontWeight: "400",
    fontSize: "1.25rem",
  },

  cardIsi: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    [theme.breakpoints.down("sm")]: {
      margin: "10px 0px",
      padding: "20px 0px",
    },
  },
  form: {
    marginTop: "30px",
  },
  radio: {
    width: "70%",
    marginBottom: "20px",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      justifyContent: "space-between",
    },
  },
  button: {
    fontFamily: "Poppins",
    fontWeight: "600",
    marginBottom: theme.spacing(1),
    borderRadius: "50px",
  },
  setuju: {
    marginLeft: "1px",
    marginBottom: "15px",
    textAlign: "left",
    marginRight: 0,
  },
  bottom: {
    marginTop: theme.spacing(2),
    fontSize: "1.15rem",
    [theme.breakpoints.down("sm")]: {
      textAlign: "center",
    },
    "& a": {
      color: "#000",
      textDecoration: "none",
      "& :hover": {
        textDecoration: "underline",
      },
    },
  },

  baris: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: "20px",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
    },
  },
  input: {
    width: "65%",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
    },
  },
  ket: {
    width: "30%",
    fontFamily: "Poppins !important",
    fontSize: "0.9rem",
    textAlign: "right",
    [theme.breakpoints.down("sm")]: {
      marginTop: theme.spacing(2),
      width: "100%",
    },
  },
}));

export default function Register(props) {
  useEffect(() => {
    document.body.bgColor = "#D2E4FF";
    document.title = "Registrasi";
    return () => {
      document.body.bgColor = "";
    };
  }, []);

  const classes = useStyles();

  const [contingent_name, setcontingent_name] = useState("")
  const [name, setname] = useState("")
  const [gender, setgender] = useState("")
  const [telp, settelp] = useState("")
  const [address, setaddress] = useState("")
  const [email, setemail] = useState("")
  const [password, setpassword] = useState("")
  const [repeatpassword, setrepeatpassword] = useState("")
  const [showpassword, setshowpassword] = useState(false)
  const [openModal, setopenModal] = useState(false)
  const [codeOTP, setcodeOTP] = useState('')
  const [isiOTP, setisiOTP] = useState('')

  const handleClose = ()=>{
    setopenModal(false)
  }

  const submitData = () =>{
      if(codeOTP === isiOTP){
        var datapostuser = {
          contingent_name,
          event_id : "5f8f857d64317f77c4e48725",
          name,
          gender,
          telp,
          address,
          email,
          password,
          user_access : 'kontingen',
        }
        console.log('datapostuser', datapostuser)
        auth.registerRequest(datapostuser).then(resuser =>{
            console.log('resuser', resuser.data)
            props.history.push('/login')
        }).catch(err=>{
          alert('Nama Kontingen sudah terdaftar')
        })
      }
      else{
        alert('Kode Verifikasi Tidak sesuai')
      }
      

  }

  function createUser(){
    if(contingent_name.length < 3){
      alert('Nama Kontingen tidak boleh kosong')
      return 
    }
    if(name.length < 3){
      alert('Nama Manager tidak boleh kosong')
      return 
    }
    if(address.length < 3){
      alert('Alamat tidak boleh kosong')
      return 
    }
    if(telp.length < 12){
      alert('nomor whatsapp tidak valid')
      return 
    }
    if(password !== repeatpassword){
      alert('Password dan Ulang Password harus sesuai')
      return 
    }
    
    if(telp.length === 12){
      var otp = randomCode(6);
      var dataOTP  = {telp, otp}
      auth.otpconfirm(dataOTP).then(res=>{
        setopenModal(true)
        setcodeOTP(otp);
        console.log('res', res)
        console.log('OTP', otp)
      }).catch(err=>{
        console.log('err', err)
      })
    }
    
    // var datacontingent = {
    //   name : contingent_name,
    //   event_id : "5f8f857d64317f77c4e48725"
    // }
    
    
    // console.log('datacontingent', datacontingent)
    
    
    // auth.registerContingentRequest(datacontingent).then(res=>{
    //   var contingent_id = res.data._id 
    //   var datapostuser = {
    //     name,
    //     gender,
    //     telp,
    //     address,
    //     email,
    //     password,
    //     user_access : 'kontingen',
    //     contingent_id : res.data._id 
    //   }
    //   auth.registerRequest(datapostuser).then(resuser =>{
    //     console.log('resuser', resuser.data)
    //     props.history.push('/login')
    //   })
    // })
  }

  function randomCode(length) {
    var result           = '';
    var characters       = '0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

  return (
    <Container maxWidth="md">
        <Dialog
          open={openModal}
          close={handleClose}
          title={<Typography style={{color:'#bf272b'}}>Masukkan Kode Verifikasi Whatsapp</Typography>}
          content={
              <>
                  <TextField value={isiOTP} onChange={(event)=> { 
                    setisiOTP(event.target.value)
                  }} />
                  <br/>
                  <br/>
                  <Button onClick={submitData} color={codeOTP === isiOTP ? "primary" : "default"} style={{marginTop:10}} style={{fontSize:'small'}} variant="contained">Kirim</Button>
              </>
          }
          cancel={handleClose}
          // confirm={  }submitData
          // valueConfirm={"Kirim"}
          valueCancel={"Tutup"}
          colorButtonConfirm={"#bf272b"}
      />
      <Card className={classes.root} variant="outlined">
        <CardContent className={classes.cardIsi}>
          <span className={classes.title}>Belum Punya Akun?</span>
          <span className={classes.subtitle}>Buat Akun Sekarang</span>
          <FormControl className={classes.form}>

            <span className={classes.baris}>
              <TextField id="outlined-basic" value={contingent_name} onChange={(event)=> setcontingent_name(event.target.value)} label="Nama Kontingen" variant="outlined" className={classes.input} required />
              <FormHelperText className={classes.ket}>Nama boleh menggunakan angka</FormHelperText>
            </span>

            <span className={classes.baris}>
              <TextField id="outlined-basic" value={name} onChange={(event)=> setname(event.target.value)} label="Nama Pemegang Akun" variant="outlined" className={classes.input} required />
              <FormHelperText className={classes.ket}>Nama hanya boleh menggunakan huruf</FormHelperText>
            </span>

            <RadioGroup row className={classes.radio}>
              <Grid item sm={6}>
                <RadioButtons ket="Pria" />
              </Grid>
              <Grid item sm={6}>
                <RadioButtons ket="Wanita" />
              </Grid>
            </RadioGroup>

            <span className={classes.baris}>
              <TextField id="outlined-basic" value={address} onChange={(event)=> setaddress(event.target.value)} label="Alamat" variant="outlined" className={classes.input} required />
              <FormHelperText className={classes.ket}>Masukan alamat lengkap Anda</FormHelperText>
            </span>
            <span className={classes.baris}>
              <TextField id="outlined-basic" type="Number" value={telp} onChange={(event)=>
                  settelp(event.target.value)
                
              } label="Nomor Whatsapp" variant="outlined" className={classes.input} required />
              <FormHelperText className={classes.ket}>Pastikan Whatsapp dalam keadaan aktif karena kami akan mengirimkan kode verifikasi</FormHelperText>
            </span>
            <span className={classes.baris}>
              <TextField id="outlined-basic" value={email} onChange={(event)=> setemail(event.target.value)} label="Email" variant="outlined" className={classes.input} required />
              <FormHelperText className={classes.ket}>Masukan alamat email yang aktif karena akan digunakan saat upload video pertandingan</FormHelperText>
            </span>

            <div className={classes.baris}>
              <FormControl className={classes.textField} variant="outlined">
                <InputLabel htmlFor="outlined-adornment-password">Kata Kunci *</InputLabel>
                <OutlinedInput
                  id="outlined-adornment-password"
                  type={showpassword ? "text" : "password"}
                  value={password}
                  onChange={(event)=>setpassword(event.target.value)}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton aria-label="toggle password visibility" onClick={()=>setshowpassword(!showpassword)} edge="end">
                        {showpassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  }
                  style={{width:'100%'}}
                />
              </FormControl>
              <FormHelperText className={classes.ket}>Kata kunci untuk masuk kedalam akun </FormHelperText>
            </div>
            <div className={classes.baris}>
              <FormControl className={classes.textField} variant="outlined">
                <InputLabel htmlFor="outlined-adornment-password">Masukan Ulang Kata Kunci *</InputLabel>
                <OutlinedInput
                  id="outlined-adornment-password"
                  type={showpassword ? "text" : "password"}
                  value={repeatpassword}
                  onChange={(event)=>setrepeatpassword(event.target.value)}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton aria-label="toggle password visibility" onClick={()=>setshowpassword(!showpassword)} edge="end">
                        {showpassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  }
                  style={{width:'100%'}}
                />
              </FormControl>
            </div>
            
            {/* <InputPassRegis /> */}
            <FormControlLabel control={<Checkbox value="accept" color="primary" />} className={classes.setuju} label="Saya menyetujui bahwa data yang saya isikan adalah benar" required />
            <div>
              <Button variant="contained" color="primary" href="#" className={classes.button} onClick={()=>createUser()}>
                Daftar
              </Button>
            </div>
          </FormControl>
          <div className={classes.bottom}>
            Sudah Punya Akun?&nbsp;
            <Link to="/login">
              <b>Klik Disini</b>
            </Link>
          </div>
        </CardContent>
      </Card>
    </Container>
  );
}
