import React, {useState, useContext, useEffect} from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Box, Button, Popover, Typography, IconButton, Grid } from "@material-ui/core";

// Icon
import GetAppIcon from "@material-ui/icons/GetApp";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { Link } from "react-router-dom";
import { PageTitle } from "../../components";
import { EventContext } from "../../context/eventcontext"
import moment from "moment"
import Dialog from '../../components/Dialog/Dialog'
import { UserContext } from "../../context/usercontext";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#039be5",
    color: theme.palette.common.white,
    fontSize: 16,
    fontFamily: "Poppins !important",
  },
  body: {
    fontSize: 14,
    fontFamily: "Poppins !important",
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles((theme) => ({
    table: {
      marginTop: "20px",
      minWidth: 700,
      "& th": {
        fontWeight: "600",
        fontSize:13
      },
    },
    button: {
      minWidth: "40px !important",
      paddingLeft: 0,
      paddingRight: 0,
    },
}));
  
export default function Pertandingan() {
    const classes = useStyles();
    const { getDataEventWithEntries, eventdata} = useContext(EventContext)
    const [event_categories, setevent_categories] = useState([])
    const [entries, setentries] = useState([])

    useEffect(() => {
        getDataEventWithEntries().then(data=>{
            if(data){
                setevent_categories(data.event_categories)
                setentries(data.entries)
            }
        });
    }, [])

    return (
        <>
            <PageTitle title="Daftar Pertandingan" />
                
            <Grid container spacing={6}>
                <Grid item md="6">
                    <Typography>Open Putra</Typography>
                    <Table size="small" aria-label="customized table">
                        <TableHead>
                            <TableRow>
                                <StyledTableCell align="center" width={20}>
                                    No
                                </StyledTableCell>
                                <StyledTableCell>Nomor Pertandingan</StyledTableCell>
                                <StyledTableCell width={80} align="center">Entries</StyledTableCell>
                                <StyledTableCell width={80} align="center"></StyledTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                event_categories.length > 0 ? event_categories.filter(x=>x.category_type === "OPEN" && x.category_gender === "L").map((item, index)=>{
                                    var entries_length =  entries.filter(x=>x._id ===item._id).map(y=>y.entries)
                                    return(
                                        <TableRow>
                                            <StyledTableCell align="center" width={20}>
                                                {index+1}
                                            </StyledTableCell>
                                            <StyledTableCell>{item.age_category + " "+ item.category_name }</StyledTableCell>
                                            <StyledTableCell align="center">{entries_length}</StyledTableCell>
                                            <StyledTableCell align="center">
                                                <Button variant="contained" color="primary">Lihat</Button>
                                            </StyledTableCell>
                                        </TableRow>
                                    )
                                }): null
                            }
                        </TableBody>
                    </Table>
                </Grid>
                <Grid item md="6">
                    <Typography>Open Putri</Typography>
                    <Table size="small" aria-label="customized table">
                        <TableHead>
                            <TableRow>
                                <StyledTableCell align="center" width={20}>
                                    No
                                </StyledTableCell>
                                <StyledTableCell>Nomor Pertandingan</StyledTableCell>
                                <StyledTableCell width={80} align="center">Entries</StyledTableCell>
                                <StyledTableCell width={80} align="center"></StyledTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                event_categories.length > 0 ? event_categories.filter(x=>x.category_type === "OPEN" && x.category_gender === "P").map((item, index)=>{
                                    var entries_length =  entries.filter(x=>x._id ===item._id).map(y=>y.entries)
                                    return(
                                        <TableRow>
                                            <StyledTableCell align="center" width={20}>
                                                {index+1}
                                            </StyledTableCell>
                                            <StyledTableCell>{item.age_category + " "+ item.category_name }</StyledTableCell>
                                            <StyledTableCell align="center">{entries_length}</StyledTableCell>
                                            <StyledTableCell align="center">
                                                <Button variant="contained" color="primary">Lihat</Button>
                                            </StyledTableCell>
                                        </TableRow>
                                    )
                                }): null
                            }
                        </TableBody>
                    </Table>
                </Grid>
            
                <Grid item md="6">
                    <Typography>Festival Putra</Typography>
                    <Table size="small" aria-label="customized table">
                        <TableHead>
                            <TableRow>
                                <StyledTableCell align="center" width={20}>
                                    No
                                </StyledTableCell>
                                <StyledTableCell>Nomor Pertandingan</StyledTableCell>
                                <StyledTableCell width={80} align="center">Entries</StyledTableCell>
                                <StyledTableCell width={80} align="center"></StyledTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                event_categories.length > 0 ? event_categories.filter(x=>x.category_type === "FESTIVAL" && x.category_gender === "L").map((item, index)=>{
                                    var entries_length =  entries.filter(x=>x._id ===item._id).map(y=>y.entries)
                                    return(
                                        <TableRow>
                                            <StyledTableCell align="center" width={20}>
                                                {index+1}
                                            </StyledTableCell>
                                            <StyledTableCell>{item.age_category + " "+ item.category_name }</StyledTableCell>
                                            <StyledTableCell align="center">{entries_length}</StyledTableCell>
                                            <StyledTableCell align="center">
                                                <Button variant="contained" color="primary">Lihat</Button>
                                            </StyledTableCell>
                                        </TableRow>
                                    )
                                }): null
                            }
                        </TableBody>
                    </Table>
                </Grid>
                <Grid item md="6">
                <Typography>Festival Putri</Typography>
                    <Table size="small" aria-label="customized table">
                        <TableHead>
                            <TableRow>
                                <StyledTableCell align="center" width={20}>
                                    No
                                </StyledTableCell>
                                <StyledTableCell>Nomor Pertandingan</StyledTableCell>
                                <StyledTableCell width={80} align="center">Entries</StyledTableCell>
                                <StyledTableCell width={80} align="center"></StyledTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                event_categories.length > 0 ? event_categories.filter(x=>x.category_type === "FESTIVAL" && x.category_gender === "P").map((item, index)=>{
                                    var entries_length =  entries.filter(x=>x._id ===item._id).map(y=>y.entries)
                                    return(
                                        <TableRow>
                                            <StyledTableCell align="center" width={20}>
                                                {index+1}
                                            </StyledTableCell>
                                            <StyledTableCell>{item.age_category + " "+ item.category_name }</StyledTableCell>
                                            <StyledTableCell align="center">{entries_length}</StyledTableCell>
                                            <StyledTableCell align="center">
                                                <Button variant="contained" color="primary">Lihat</Button>                                                
                                            </StyledTableCell>
                                        </TableRow>
                                    )
                                }): null
                            }
                        </TableBody>
                    </Table>
                </Grid>
            </Grid>
        </>
    )
}
