import React from "react";
import {
  Route,
  Switch,
  withRouter,
} from "react-router-dom";
import Login from "../pages/login/Login"
import Register from "../pages/register/Register"

function LayoutPublic() {

  return (
    <>
    <Switch> 
        <Route path="/login" component={Login} />
        <Route path="/registration" component={Register} />
    </Switch>
    </>
  );
}

export default withRouter(LayoutPublic);
