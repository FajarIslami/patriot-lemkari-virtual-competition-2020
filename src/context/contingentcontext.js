import React, {useState, createContext, useReducer, useEffect} from 'react'
import api from "../services/api"
import _ from 'lodash';


export const ContingentContext = createContext()
const ContingentContextProvider = (props) =>{
    
    const [contingent, contingentdispatch] = useReducer(ContextReducer, {}) 
    const [contingentall, contingentalldispatch] = useReducer(ContextAllReducer, []) 
    
    function getDataContingent(contingent_id){
        return api.getContingent(contingent_id).then(res=>
            {
                contingentdispatch({type:'SET_DATA', data:res.data})
                return res.data
            }
        )
    }

    function getAllDataContingent(){
        return api.getAllContingent().then(res=>
            {
                console.log('get all contingen', res.data)
                contingentalldispatch({type:'SET_DATA', data:res.data})
                return res.data
            }
        )
    }

    function deleteDataContingent(id){
        return api.deleteContingent(id).then(res=>
            {
                console.log('res', res.data)
                contingentalldispatch({type:'DELETE_DATA', id})
                return res.data
            }
        )
    }

    return(
        <ContingentContext.Provider value={{ getDataContingent, getAllDataContingent, deleteDataContingent, contingent, contingentall, contingentdispatch}}>
            { props.children }
        </ContingentContext.Provider>
    )
}
const ContextReducer = (state, action)=>{
    switch(action.type){
        case 'SET_DATA': 
            return state = action.data
        case 'ADD_DATA': 
            return state = [...state, action.data]
        default: {
            return state
        }
    }
}

const ContextAllReducer = (state, action)=>{
    switch(action.type){
        case 'SET_DATA': 
            return state = action.data
        case 'ADD_DATA': 
            return state = [...state, action.data]
        case 'DELETE_DATA': 
            return state = state.filter(x=>x._id != action.id)
        default: {
            return state
        }
    }
}

export default ContingentContextProvider

