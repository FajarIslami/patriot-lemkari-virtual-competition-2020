import React, {useState, useContext, useEffect, createContext, useReducer} from 'react'
import api from "../services/api"
import _ from 'lodash';
import { UserContext } from "./usercontext"
export const InvoiceContext= createContext()

const InvoiceContextProvider = (props) =>{    
    const [invoicedata, invoicedatadispatch] = useReducer(ContextReducer, []) 
    const [invoice_detail, setinvoice_detail] = useState({})
    const [entrie_detail, setentrie_detail] = useState([])
    const { getDataUser } = useContext(UserContext)
    const [userdata, setuserdata] = useState([])

    useEffect(() => {
        getDataUser().then(data=>{
            if(data){
                setuserdata(data)
            }
        });
    }, [])
    
    function getDataInvoice(contingent_id){
        var id = "5f8f857d64317f77c4e48725"
        if(contingent_id){
            return api.getInvoiceByContingent(contingent_id, id).then(res=>
                {
                    console.log('invoice', JSON.stringify(res.data))
                    invoicedatadispatch({type:'SET_DATA', data:res.data})
                    return res.data
                }
            )
        }
        else{
            return api.getInvoiceByEvent(id).then(res=>
                {
                    console.log('invoice', JSON.stringify(res.data))
                    invoicedatadispatch({type:'SET_DATA', data:res.data})
                    return res.data
                }
            )
        }
        
    }

    function getDataInvoiceDetail(invoice_number){
        api.getInvoiceByNumber(invoice_number).then(res=>{
            console.log(res.data)
            setinvoice_detail(res.data.length> 0 ? res.data[0] : {})
        })
        api.postInvoiceDetail(invoice_number).then(res=>{
            console.log('entrie_detail', res.data)
            setentrie_detail(res.data)
        })
    }

    function makeid(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
     }

    async function postDataInvoice(payment_list, contingent_data, event_data, invoice_detail, total){
        var invoice_number =  makeid(8);
        console.log('contingent_data', contingent_data._id)
        
        var getuser = await getDataUser().then(data=>{
            if(data){
                return data.filter(x=>x.contingent_id === contingent_data._id)[0]
            }
        });
        console.log('get user', getuser)
        var datauser = localStorage.user_access === "Admin" ?  getuser : JSON.parse(localStorage.user)
        var datapost = {
            invoice_number : invoice_number,
            invoice_date : new Date(),
            invoice_from: {
                name : datauser.name,
                contingent_name :  contingent_data.name,
                telp : datauser.telp
            },
            invoice_to : {
                bank_account : event_data.invoice_info ? event_data.invoice_info.bank_account : "-",
                bank_name : event_data.invoice_info ? event_data.invoice_info.bank_name : "-",
                name : event_data.invoice_info ? event_data.invoice_info.account_holder : "-",
                telp :  event_data.invoice_info ? event_data.invoice_info.contact : "-"
            },
            event_id : event_data._id,
            event_name : event_data.event_name,
            contingent_id : contingent_data._id,
            contingent_name : contingent_data.name,
            invoice_detail : invoice_detail,
            invoice_total : total,
            invoice_status : "Belum Bayar",
            invoice_due : new Date(),
        }
        console.log('datapost', datapost)
        return api.postInvoice(datapost).then(res=>
            {
                api.putCheckout(invoice_number, payment_list)
                console.log('invoice', JSON.stringify(res.data))
                invoicedatadispatch({type:'ADD_DATA', data:res.data})
                return res.data
            }
        )
    }

    function confirm_invoice(id, data){
        api.putInvoice(id, data).then(res=>{
            console.log('confirm_invoice', res.data)
            invoicedatadispatch({type:'CONFIRM_PAYMENT', data:res.data})
        })
    }

    function putDataReceivePayment(invoice_number){
        return api.putreceivePayment(invoice_number).then(res=>{
            console.log('receivePayment', res.data)
            getDataInvoiceDetail(invoice_number)
            //invoicedatadispatch({type:'CONFIRM_PAYMENT', data:res.data})
            return res.data
        })
    }

    function putDataResetPayment(invoice_number){
        console.log('resetPayment invoice_number', invoice_number)
        return api.putresetPayment(invoice_number).then(res=>{
            console.log('resetPayment', res.data)
            getDataInvoiceDetail(invoice_number)
            //invoicedatadispatch({type:'CONFIRM_PAYMENT', data:res.data})
            return res.data
        })
    }

    return(
        <InvoiceContext.Provider value={{ getDataInvoice, postDataInvoice, getDataInvoiceDetail, confirm_invoice, putDataReceivePayment, putDataResetPayment, entrie_detail, invoice_detail, invoicedata, invoicedatadispatch}}>
            { props.children }
        </InvoiceContext.Provider>
    )
}
const ContextReducer = (state, action)=>{
    switch(action.type){
        case 'SET_DATA': 
            return state = action.data
        case 'ADD_DATA': 
            return state = [...state, action.data]
        case 'CONFIRM_PAYMENT': 
            return state = state.map(x=>{if(x._id === action.data._id){return action.data}else{return x}});
        default: {
            return state
        }
    }
}

export default InvoiceContextProvider

