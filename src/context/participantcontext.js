import React, {useState, createContext, useReducer, useEffect} from 'react'
import api from "../services/api"
import _ from 'lodash';
import { InvoiceContext } from "./invoicecontext"
export const ParticipantContext= createContext()

const ParticipantContextProvider = (props) =>{
    
    const [participant, participantdispatch] = useReducer(ContextReducer, [])     
    const [participantdetail, setparticipantdetail] = useState({})
    const [loadingparticipant, setloadingparticipant] = useState(false)
    // const { getDataUser } = useContext(InvoiceContext)
    async function getDataAthlete(contingent_id){
        setloadingparticipant(true)
        var event_id = "5f8f857d64317f77c4e48725"
        var datacategories = await getDataCategoriesAthlete(contingent_id, event_id)
        var dataatletes = contingent_id ? 
                            await api.getAthlete(contingent_id, event_id, "Athlete").then(res=>res.data) :
                            await api.getAthleteByEvent(event_id, "Athlete").then(res=>res.data)
        console.log('datacategories', datacategories)
        await dataatletes.map(x=>{
           var datacategory = datacategories.filter(y=>y.participant_id === x._id)
           if(datacategory.length >0){
               x.categories =  datacategory[0].categories
           }
           else{
               x.categories = []
           }
        })
        console.log('dataatletes', dataatletes)
        participantdispatch({type:'SET_DATA', data:dataatletes})
        setloadingparticipant(false)
        return dataatletes        
    }

    function getDataCategoriesAthlete(contingent_id, event_id){
        if(localStorage.user_access === "Admin"){
            return api.getCategoriesAthleteByEvent(event_id).then(res=>res.data)            
        }
        else{
            return api.getCategoriesAthlete(contingent_id, event_id).then(res=>res.data)
        }
    }

    async function getDetailAthlete(id){
        var dataatletes = await api.getAthleteDetail(id).then(res=>res.data)
        var datacategories = await api.getCategoriesAthleteDetail(id).then(res=>res.data)
        await dataatletes.map(x=>{
            var datacategory = datacategories.filter(y=>y.contingent_id === x.contingent_id && y.participant_id === x._id)
            if(datacategories.length >0){
                x.categories =  datacategory[0].categories
            }
            else{
                x.categories = []
            }
         })
         setparticipantdetail(dataatletes[0])
         return dataatletes[0]
    }

    function postDataParticipant(data){
        return api.postParticipant(data).then(res=>{
            var datapertandingan = {
              event_id : data.event_id,
              contingent_id: res.data.contingent_id,
              participant_id : res.data._id,
              categories : data.categories,
              register : {
                  register_date : new Date(),
                  register_by : localStorage.user_access
              },
              register_status : true
            }
            api.postEntrieAthlete(datapertandingan).then(respertandingan=>{
                getDataAthlete(datapertandingan.contingent_id)
                return true
            })
        })
    }

    function putDataParticipant(id, data){
        return api.putParticipant(id, data).then(res=>{
            var datapertandingan = {
              categories : data.categories,
            }
            console.log('datapertandingan', datapertandingan)
            api.putEntrieAthlete(id, datapertandingan).then(respertandingan=>{
              console.log('respertandingan', respertandingan.data)
              getDataAthlete(datapertandingan.contingent_id)
              return true
            })
        })
    }

    function deleteDataParticipant(id, contingent_id, invoice_number){
        
        return api.deleteParticipant(id).then(async res=>{
            getDataAthlete(contingent_id)
            await api.putresetPayment(invoice_number)
            return true
        })
    }

    return(
        <ParticipantContext.Provider value={{ getDataAthlete, getDetailAthlete, getDataCategoriesAthlete, 
                                              postDataParticipant, putDataParticipant, deleteDataParticipant,
                                              participantdetail, loadingparticipant, participant, participantdispatch}}>
            { props.children }
        </ParticipantContext.Provider>
    )
}
const ContextReducer = (state, action)=>{
    switch(action.type){
        case 'SET_DATA': 
            return state = action.data
        case 'ADD_DATA': 
            return state = [...state, action.data]
        case 'DELETE_DATA': 
            return state = [...state, action.data]
        default: {
            return state
        }
    }
}

export default ParticipantContextProvider

