import React, {useState, createContext, useReducer, useEffect} from 'react'
import api from "../services/api"
import _ from 'lodash';
export const EventContext= createContext()
const EventContextProvider = (props) =>{
    
    const [eventdata, eventdatadispatch] = useReducer(ContextReducer, {})     
    
    // useEffect(() => {
    //     if(localStorage.getItem("token")){
    //         return setisAuthenticated(true)
    //     }
    //     return setisAuthenticated(false)
    // }, [])

    async function getDataEvent(){
        var id = "5f8f857d64317f77c4e48725"
        return api.getEvent(id).then(async res=>
            {
                console.log('event', JSON.stringify(res.data))
                eventdatadispatch({type:'SET_DATA', data:res.data})
                return res.data
            }
        )
    }

    async function getDataEventWithEntries(){
        var id = "5f8f857d64317f77c4e48725"
        return api.getEvent(id).then(async res=>
            {
                console.log('event', JSON.stringify(res.data))
                var datacategory = await api.getEntriesEvent(id).then(res=>res.data)   
                var dataevent = {...res.data, entries : datacategory}
                console.log('dataevent', dataevent) 
                eventdatadispatch({type:'SET_DATA', data: dataevent})
                return dataevent
            }
        )
    }

    return(
        <EventContext.Provider value={{ getDataEventWithEntries, getDataEvent, eventdata, eventdatadispatch}}>
            { props.children }
        </EventContext.Provider>
    )
}
const ContextReducer = (state, action)=>{
    switch(action.type){
        case 'SET_DATA': 
            return state = action.data
        case 'ADD_DATA': 
            return state = [...state, action.data]
        default: {
            return state
        }
    }
}

export default EventContextProvider

