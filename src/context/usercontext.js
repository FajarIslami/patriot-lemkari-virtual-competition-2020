import React, {useState, createContext, useReducer, useEffect} from 'react'
import auth from "../services/auth"
import api from "../services/api"
import _ from 'lodash';
export const UserContext= createContext()

const UserContextProvider = (props) =>{
    
    const [user, userdispatch] = useReducer(ContextReducer, [])     
    const [isAuthenticated, setisAuthenticated] = useState(false)

    useEffect(() => {
        if(localStorage.getItem("token")){
            return setisAuthenticated(true)
        }
        else{
            return setisAuthenticated(false)
        }
    }, [isAuthenticated])

    function userlogin(email, password){
        var datapost = {email, password}
        auth.authRequest(datapost).then(res=>
            {
                console.log('login', res.data)
                localStorage.setItem('token', res.data.token)
                localStorage.setItem('contingent_id', res.data.data.contingent_id)
                localStorage.setItem('user_access', res.data.data.user_access)
                localStorage.setItem('user', JSON.stringify(res.data.data))
                setisAuthenticated(true)
            }
        )
        
    }

    function getDataUser(){
        return api.getUser().then(res=>
            {
                userdispatch({type:'SET_DATA', data:res.data})
                return res.data
            }
        )
    }

    function userlogout(){
        localStorage.clear()
        setisAuthenticated(false)
    }
    // const getDataUser = () =>{
    //     return api.create().getUser().then(res=>{
    //         userdispatch({type:'SET_DATA', data: res.data});
    //         return res.data
    //     })
    // }

    // const postDataUser = (data) =>{
    //     return api.create().userRegister(data).then(res=>{
    //         console.log('res.data', res.data)
    //         userdispatch({type:'ADD_DATA', data: res.data});
    //         return res.data
    //     })
    // }

    return(
        <UserContext.Provider value={{isAuthenticated,userlogin, userlogout, getDataUser, user, userdispatch}}>
            { props.children }
        </UserContext.Provider>
    )
}
const ContextReducer = (state, action)=>{
    switch(action.type){
        case 'SET_DATA': 
            return state = action.data
        case 'ADD_DATA': 
            return state = [...state, action.data]
        default: {
            return state
        }
    }
}

export default UserContextProvider

