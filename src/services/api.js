import axios from 'axios';
import StaticVar from '../config/staticvar';

// ===> api create 
const api = axios.create({
  baseURL: StaticVar.Base_Url,
  timeout: 10000,
  headers:{}
});

// ===> api interceptors 
api.interceptors.request.use(function (config) {
    // set headers after authentication
    config.headers['token'] = localStorage.getItem("token");
    return config;
}, function (error) {
  // Do something with request error
  return Promise.reject(error);
});

// ===> api list function request
const getEvent = (id) => api.get('/events/'+id);
const getEntriesEvent = (id) => api.get('/events/entries/'+id);
const getUser = () => api.get('/users');
const getAllContingent = () => api.get('/contingents');
const deleteContingent = (id) => api.delete('/contingents/'+id);
const deleteParticipant = (id) => api.delete('/participants/'+id);
const getContingent = (id) => api.get('/contingents/'+id);
const getAthlete = (contingent_id, event_id) => api.get('/participants/'+contingent_id+'/'+event_id+'/Athlete');
const getAthleteByEvent = (event_id) => api.get('/participants/event_id='+event_id+'&participant_type=Athlete');
const getAthleteDetail = (id) => api.get('/participants/'+id);
const getCategoriesAthlete = (contingent_id, event_id) => api.get('/entrie_athletes/'+contingent_id+'/'+event_id);
const getCategoriesAthleteByEvent = (event_id) => api.get('/entrie_athletes?event_id='+event_id);
const getCategoriesAthleteDetail = (id) => api.get('/entrie_athletes?participant_id='+id);
const postParticipant = (data) => api.post('/participants/create', data);
const putParticipant = (id,data) => api.put('/participants/'+id, data);
const postEntrieAthlete = (data) => api.post('/entrie_athletes/create', data);
const putEntrieAthlete = (id, data) => api.put('/entrie_athletes/'+id, data);
const postCheckout = (id) => api.post('/entrie_athletes/checkout/'+id);
const postInvoiceDetail = (invoice_number) => api.post('/entrie_athletes/invoice/'+invoice_number);
const getInvoiceByContingent = (contingent_id, event_id) => api.get('/invoices?contingent_id='+contingent_id+'&event_id='+event_id);
const getInvoiceByEvent = (event_id) => api.get('/invoices?event_id='+event_id);
const getInvoiceByNumber = (invoice_number) => api.get('/invoices?invoice_number='+invoice_number);
const postInvoice = (data) => api.post('/invoices/create', data);
const putInvoice = (id, data) => api.put('/invoices/'+id, data);
const putreceivePayment = (invoice_number) => api.put('/invoices/receive/'+invoice_number);
const putresetPayment = (invoice_number) => api.put('/invoices/reset/'+invoice_number);
const putCheckout = (invoice_number, data) => api.put('/entrie_athletes/checkout/'+invoice_number, data);

export const apis = {
    getEvent,
    getEntriesEvent,
    getUser,
    getContingent,
    getAllContingent,
    deleteContingent,
    deleteParticipant,
    getAthlete,
    getAthleteByEvent,
    postParticipant,
    putParticipant, 
    postEntrieAthlete,
    getCategoriesAthlete,
    getCategoriesAthleteByEvent,
    getAthleteDetail,
    getCategoriesAthleteDetail,
    putEntrieAthlete,
    postCheckout,
    putCheckout,
    getInvoiceByContingent,
    getInvoiceByEvent,
    getInvoiceByNumber,
    postInvoiceDetail,
    putInvoice,
    postInvoice,
    putresetPayment,
    putreceivePayment
}

export default apis