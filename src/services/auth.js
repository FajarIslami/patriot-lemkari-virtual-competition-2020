import axios from 'axios';
import StaticVar from '../config/staticvar';

// ===> api create 
const api = axios.create({
  baseURL: StaticVar.Base_Url,
  timeout: 10000,
  headers:{}
});

// ===> api list function request
const authRequest = (data) => api.post('/auth/login', data);
const registerRequest = (data) => api.post('/auth/register', data);
const otpconfirm = (data) => api.post('/auth/otp', data);
const registerContingentRequest = (data) => api.post('/auth/addcontingent', data);


export const apis = {
    authRequest,
    registerRequest,
    otpconfirm,
    registerContingentRequest
}

export default apis