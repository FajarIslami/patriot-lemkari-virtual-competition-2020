import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import MultiProvider from "./config/MultiProvider"
import UserProvider from './context/usercontext';
import EventProvider from './context/eventcontext';
import ParticipantProvider from './context/participantcontext';
import ContingentProvider from './context/contingentcontext';
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import InvoiceProvider from "./context/invoicecontext";


ReactDOM.render(
    <MultiProvider
        providers={[
            <EventProvider />,
            <ParticipantProvider/> ,
            <ContingentProvider/>,
            <InvoiceProvider/>,
            <UserProvider/>,
        ]}>
        <App />
    </MultiProvider>
, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
