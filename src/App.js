import React, {useContext} from 'react';

import { Route, Redirect, Switch,  BrowserRouter as Router } from "react-router-dom";
import LayoutPrivate from "./routes/LayoutPrivate"
import LayoutPublic from "./routes/LayoutPublic"
import { UserContext } from "./context/usercontext";

function PrivateRoute({ component, ...rest }) {
    var { isAuthenticated } = useContext(UserContext);
    console.log('Private', isAuthenticated)
    return (
      <Route
        {...rest}
        render={props =>
          isAuthenticated || localStorage.getItem('token') ? (
            React.createElement(component, props)
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: {
                  from: props.location,
                },
              }}
            />
          )
        }
      />
    );
}

function PublicRoute({ component, ...rest }) {
    var { isAuthenticated } = useContext(UserContext);
    console.log('Public', isAuthenticated)
    return (
      <Route
        {...rest}
        render={props =>
          isAuthenticated || localStorage.getItem('token') ? (
            <Redirect
              to={{
                pathname: "/app/dashboard",
              }}
            />
          ) : (
            React.createElement(component, props)
          )
        }
      />
    );
}


function App() {    
    return (
      <Router>
        <Switch>
            <Route exact path="/" render={() => <Redirect to="/login" />} />
            <Route exact path="/app" render={() => <Redirect to="/app/dashboard" />} />
            <PrivateRoute path="/app" component={LayoutPrivate} />
            <PublicRoute path="/" component={LayoutPublic} />
            <Route component={Error} />
        </Switch>
      </Router>
    );
}

export default App;
